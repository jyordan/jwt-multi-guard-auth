<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        App\User::truncate();
        App\Member::truncate();

        factory(App\User::class, 10)->create();
        factory(App\User::class)->create(['email' => 'josephuser@gmail.com', 'password' => bcrypt('itdev123')]);
        factory(App\Member::class, 10)->create();
        factory(App\Member::class)->create(['email' => 'josephmember@gmail.com', 'password' => bcrypt('itdev123')]);

        Model::reguard();
    }
}
