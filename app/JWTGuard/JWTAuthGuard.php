<?php

namespace App\JWTGuard;

use \Tymon\JWTAuth\JWTAuth as JWTAuth;

class JWTAuthGuard extends JWTAuth
{
    public function attempt(array $credentials = [], array $customClaims = [])
    {
        $customClaims['guard'] = $this->guardDefault();
        if (! $this->auth->byCredentials($credentials)) {
            return false;
        }

        return $this->fromUser($this->auth->user(), $customClaims);
    }

    public function getGuard($token = false)
    {
        $payload = $this->getPayload($token);

        if(array_has($payload, 'guard'))
        {
            return $payload['guard'];
        }

        return false;
    }

    public function guard($guard = null)
    {
        if($guard)
        {
            \Config::set('auth.defaults.guard', $guard);
        }

        return $this;
    }

    public function guardDefault()
    {
        return \Config::get('auth.defaults.guard');
    }
}
