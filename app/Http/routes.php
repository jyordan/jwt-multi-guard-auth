<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'api'], function()
{
	Route::get('authenticate', 'AuthenticateController@index')->middleware('jwt.auth:user');
	Route::get('authenticate/member', ['middleware' => ['jwt.auth:user|member'], 'uses' => 'AuthenticateController@member']);
	Route::post('authenticate', 'AuthenticateController@authenticate');
});
